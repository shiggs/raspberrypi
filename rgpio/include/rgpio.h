/*
 * This is a custom gpio kernel module for the raspberry pi
 * Copyright (C) 2012 Stephen Higgs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 	
 */
#ifndef RGPIO_H
#define RGPIO_H

#include <linux/types.h> /* size_t */

//module major number
int rgpio_port = 61;
uint32_t RGPIO_IRQ1 = 0x31;
uint32_t RGPIO_IRQ2 = 0x32;
uint32_t RGPIO_IRQ3 = 0x33;
uint32_t RGPIO_IRQ4 = 0x34;

static const uint32_t PERIPHERAL_PHYSICAL_BASE = 0xF2000000;
static const uint32_t GPIO_PHYSICAL_BASE = 0xF2200000;
static const uint32_t INTERRUPT_BASE = 0x2000B000;
static const uint32_t INTERRUPT_MAPPED_SIZE = 0xFFF;
static const uint32_t INTERRUPT_REG_ENABLE1_OFFSET = 0x210;
static const uint32_t INTERRUPT_REG_ENABLE2_OFFSET = 0x214;

static const char CMD_OUT = 0x01;
static const char CMD_IN = 0x02;
static const char CMD_HIGH = 0x03;
static const char CMD_LOW = 0x04; 
static const char CMD_DETECT_RISING = 0x05;
static const char CMD_INT_PID = 0x0F;
//...
static const uint32_t MODE_OUT = 0x01;
static const uint32_t MODE_IN = 0x00;

#endif
