#include <stdio.h>
#include <signal.h>
#include <unistd.h>

static const char CMD_OUT = 0x01;
static const char CMD_IN = 0x02;
static const char CMD_HIGH = 0x03;
static const char CMD_LOW = 0x04;
static const char CMD_DETECT_RISING = 0x05;
static const char CMD_INT_PID = 0x0F;
static const char PIN_18 = 0x12;
static const char PIN_25 = 0x19;


void local_sig_handler(int signum) {
	printf("received a signal from rgpio module\n");
	fflush(stdout);
}

int main() {

	signal(SIGIO, local_sig_handler);

	FILE *fp;

	fp= fopen("/dev/rgpio", "w");


	if (fp == NULL) {
         printf("could not open the device for writing.\n");
         return 0;
	}

	fputc(0x00, fp);
	fputc(CMD_INT_PID, fp);
	fflush(fp);

	fputc(PIN_18, fp);
	fputc(CMD_OUT, fp);
	fflush(fp);

	fputc(PIN_25, fp);
	fputc(CMD_OUT, fp);
	fflush(fp);

	fputc(PIN_25, fp);
	fputc(CMD_LOW, fp);
	fflush(fp);

	fputc(PIN_25, fp);
	fputc(CMD_IN, fp);
	fflush(fp);

	fputc(PIN_25, fp);
	fputc(CMD_DETECT_RISING, fp);
	fflush(fp);

	fputc(PIN_18, fp);
	fputc(CMD_HIGH, fp);
	fflush(fp);

	fputc(PIN_18, fp);
	fputc(CMD_LOW, fp);
	fflush(fp);

	return 0;
}
