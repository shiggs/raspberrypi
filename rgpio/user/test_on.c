#include <stdio.h>

static const char CMD_OUT = 0x01;
static const char CMD_IN = 0x02;
static const char CMD_HIGH = 0x03;
static const char CMD_LOW = 0x04;

static const char PIN_18 = 0x12;

int main() {

	FILE *fp;

	fp= fopen("/dev/rgpio", "w");


	if (fp == NULL) {
         printf("could not open the device for writing.\n");
         return 0;
	}

	fputc(PIN_18, fp);
	fputc(CMD_OUT, fp);
	fflush(fp);

	fputc(PIN_18, fp);
	fputc(CMD_HIGH, fp);
	fflush(fp);

	sleep(5);

	fputc(PIN_18, fp);
	fputc(CMD_LOW, fp);
	fflush(fp);

	return 0;
}
