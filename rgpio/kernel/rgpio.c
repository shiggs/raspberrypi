/*
 * This is a custom gpio kernel module for the raspberry pi
 * Copyright (C) 2012 Stephen Higgs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This source was created using examples from: 
 * 	http://www.freesoftwaremagazine.com/articles/drivers_linux
 *		Xavier Calbet
 *	http://www.tldp.org/LDP/lkmpg/2.4/html/c577.htm
 *		Peter Jay Salzman/Rodrigo Rubira Branco
 *	http://elinux.org/RPi_Low-level_peripherals
 * 	http://www.raspberrypi.org/phpBB3/viewtopic.php?f=33&t=8476
 *
 * References:
 * 	http://www.raspberrypi.org/wp-content/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
 *
 * 	
 */

/* Necessary includes for drivers */

#include "rgpio.h"
#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h> /* strcpy() */
#include <linux/kernel.h> /* printk() */
#include <linux/slab.h> /* kmalloc() */
#include <linux/fs.h> /* everything... */
#include <linux/errno.h> /* error codes */
#include <linux/proc_fs.h>
#include <linux/fcntl.h> /* O_ACCMODE */
#include <linux/ioport.h>
#include <linux/workqueue.h>
#include <linux/interrupt.h>	/* We want an interrupt */
#include <linux/sched.h>
#include <asm/system.h> /* cli(), *_flags */
#include <asm/uaccess.h> /* copy_from/to_user */
#include <asm/io.h> /* inb, outb */
#include <linux/delay.h>

MODULE_LICENSE("Dual BSD/GPL");

char *gpio_out_buff;
char *gpio_out_buff_ptr;
int signal_pid = -1;
static const int SIG_TEST = 44;

static volatile uint32_t *gpio;

//forward declarations
int rgpio_open(struct inode *inode, struct file *filp); 
int rgpio_release(struct inode *inode, struct file *filp); 
ssize_t rgpio_read(struct file *filp, char *buffer, size_t length, loff_t *offset); 
ssize_t rgpio_write(struct file *filp, const char *bufer, size_t length, loff_t *offset); 
void rgpio_exit(void);
int rgpio_init(void);
void setPinMode(int pin, uint32_t mode);
void setPinOutputHigh(int pin);
void setPinOutputLow(int pin);
void clearAllDetects(void);
void setPinDetectRising(int pin);
void outputInterruptRegs(void);
void clearEventStatus(int pin);
void rgpio_enable_irq(int irq);
irqreturn_t irq_handler(int irq, void *dev_id);

struct file_operations rgpio_fops = { 
read: rgpio_read,
      write: rgpio_write,
      open: rgpio_open,
      release: rgpio_release
};

static struct workqueue_struct *my_workqueue;

//register init/exit fxn pointers with kernel
module_init(rgpio_init);
module_exit(rgpio_exit);

int rgpio_init(void) {

	int result;
	rgpio_enable_irq(RGPIO_IRQ1);
	rgpio_enable_irq(RGPIO_IRQ2);
	rgpio_enable_irq(RGPIO_IRQ3);
	rgpio_enable_irq(RGPIO_IRQ4);

	result = register_chrdev(rgpio_port, "rgpio", &rgpio_fops);
	if(result < 0) {
		printk("<1>cannot obtain port for raspberry gpio port %d\n", rgpio_port);
		return 0;
	} else {
		printk("<1>registered character device\n");
	}

	my_workqueue = create_workqueue("rgpio");
	printk("<1>created work queue\n");
	result = request_irq ( 	RGPIO_IRQ1,	
				irq_handler,
				IRQF_SHARED, 	
				"rgpio_irq_handler",
				&RGPIO_IRQ1 );

	if(result < 0) {
		printk("<1>error requesting irq: %d\n", result);
	} else {
		printk("<1>successfully request irq: %d\n", RGPIO_IRQ1);
	}

	printk("<1>allocating buffers\n");
	gpio_out_buff = kmalloc(100, GFP_KERNEL); //test
	gpio = (uint32_t *)GPIO_PHYSICAL_BASE;

	clearAllDetects();

	printk("<1>Inserting rgpio module\n"); 
	printk("<1>The GPIO pointer is pointing to addr: %p\n", gpio);
	return 0;	
}

void rgpio_exit(void) {
	
	free_irq(RGPIO_IRQ1, &RGPIO_IRQ1);
	unregister_chrdev(rgpio_port, "rgpio");
	kfree(gpio_out_buff);
	printk("<1>Removing rgpio module\n");
}


int rgpio_open(struct inode *inode, struct file *filp) {

	//always success
	strcpy(gpio_out_buff, "hello world\n");
	gpio_out_buff_ptr = gpio_out_buff;
	return 0;
}


int rgpio_release(struct inode *inode, struct file *filp) {

	//always success
	return 0;
} 


ssize_t rgpio_read(struct file *filp, 
		char *buffer, 	/* The buffer to fill with data */
		size_t length, 	/* The length of the buffer     */
		loff_t *offset)	/* Our offset in the file       */ 
{

	int bytes_read = 0;

	if(*gpio_out_buff_ptr == 0) {
		return 0;
	}

	//do a copy_to_user
	while(length && *gpio_out_buff_ptr) {
		put_user(*gpio_out_buff_ptr, buffer); //copy byte to user's buffer
		gpio_out_buff_ptr++;
		buffer++;
		length--;
		bytes_read++;
	}


	return  bytes_read;
}

ssize_t rgpio_write(struct file *filp, const char *buffer, size_t length, loff_t *offset) {


	char b1 = 0x00;
	char b2 = 0x00;

	if(length == 2) {
		b1 = *buffer;
		b2 = *(buffer+1);
	}

	if(b2 == CMD_OUT) {
		setPinMode((int) b1, MODE_OUT);
	} else if (b2 == CMD_HIGH) {
		setPinOutputHigh((int) b1);
	} else if (b2 == CMD_LOW) {
		setPinOutputLow((int) b1);
	} else if (b2 == CMD_IN) {
		setPinMode((int) b1, MODE_IN);
	} else if (b2 == CMD_DETECT_RISING) {
		setPinDetectRising((int) b1);
	} else if (b2 == CMD_INT_PID) {
		printk("registering a new process id to receive signals: %d\n", current->pid);
		signal_pid = current->pid;
	} else {
		printk("<1>command not understood: %02X", b2);
	}

	return length;
} 


void setPinMode( int pin, uint32_t mode )
{

        //Choose the right 'select' register address for this pin
        //GPFSEL0 = pins 0 - 9 = base + 0 bytes
        //GPFSEL1 = pins 10-19 = base + 4 bytes
        //GPFSEL2 = pins 20-29 = base + 8 bytes
        //1 offset unit = 32 bits (or 4 bytes)
        int offset = pin / 10;
        int remainder = pin % 10;

        //create a pointer to memory address of the 'select' register
        //that is appropriate for this pin
        volatile uint32_t *selAddr = (gpio + offset);

        //use the selected register to set the pin to 'output'
        //000 = GPIO Pin is an input
        //001 = GPIO Pin is an output ( = 0x07 )
        //100 = GPIO Pin takes alternate function 0
        //101 = GPIO Pin takes alternate function 1
        //110 = GPIO Pin takes alternate function 2
        //111 = GPIO Pin takes alternate function 3
        //011 = GPIO Pin takes alternate function 4
        //010 = GPIO Pin takes alternate function 5
        uint32_t fsel = mode;

        //create a mask for the relevant bits in the register
        //for this pin
        uint32_t mask = (0x07 << (3 * remainder));

        //shift 'select' to the right location in the register for 'pin'
        //e.g. pin 1 select is bits 3-5 in the select register
        //fsel = function select
        fsel = fsel << (3 * remainder);

        //Put the selection bits into the 'select' register.
        //This is tricky, but it does merge the bits from selAddr and fsel
        //according to the mask.  The other bits are thus preserved.
        //ref: http://graphics.stanford.edu/~seander/bithacks.html#MaskedMerge
        (*selAddr) = (*selAddr) ^ (((*selAddr) ^ fsel) & mask);

        return;
}

void setPinOutputHigh(int pin)
{
        volatile uint32_t *outSelAddr0 = (gpio + 7);
        volatile uint32_t *outSelAddr1 = (gpio + 8);

        if(pin < 32) {
                (*outSelAddr0) = (0x01 << pin);
        }
        else {
                (*outSelAddr1) = (0x01 << (pin - 32));
        }

        return;
}

void setPinOutputLow(int pin)
{
        volatile uint32_t *outClrAddr0 = (gpio + 10);
        volatile uint32_t *outClrAddr1 = (gpio + 11);

        if (pin < 32) {
                (*outClrAddr0) = (0x01 << pin);
        } else {
                (*outClrAddr1) = (0x01 << (pin - 32));
        }

        return;
}

void setPinDetectRising(int pin)
{
	//rising detect registers
        volatile uint32_t *highDetectAddr0 = (gpio + 19);
        volatile uint32_t *highDetectAddr1 = (gpio + 20);

        if(pin < 32) {
                (*highDetectAddr0) = (0x01 << pin);
        }
        else {
                (*highDetectAddr1) = (0x01 << (pin - 32));
        }

        return;
}

void clearAllDetects(void) {
	volatile uint32_t *risingDetectAddr0 = (gpio + 19);
        volatile uint32_t *risingDetectAddr1 = (gpio + 20);
	volatile uint32_t *fallingDetectAddr0 = (gpio + 22);
        volatile uint32_t *fallingDetectAddr1 = (gpio + 23);
	volatile uint32_t *highDetectAddr0 = (gpio + 25);
        volatile uint32_t *highDetectAddr1 = (gpio + 26);
	volatile uint32_t *lowDetectAddr0 = (gpio + 28);
        volatile uint32_t *lowDetectAddr1 = (gpio + 29);

	*risingDetectAddr0 = 0x00;
	*risingDetectAddr1 = 0x00;
	*fallingDetectAddr0 = 0x00;
	*fallingDetectAddr1 = 0x00;
	*highDetectAddr0 = 0x00;
	*highDetectAddr1 = 0x00;
	*lowDetectAddr0 = 0x00;
	*lowDetectAddr1 = 0x00;
}

void clearEventStatus(int pin) {

        volatile uint32_t *eventAddr0 = (gpio + 16);
        volatile uint32_t *eventAddr1 = (gpio + 17);

        if(pin < 32) {
                (*eventAddr0) = (0x01 << pin);
        }
        else {
                (*eventAddr1) = (0x01 << (pin - 32));
        }

}


irqreturn_t irq_handler(int irq, void *dev_id)
{

	struct siginfo info;
	struct task_struct *t;
	info.si_signo=SIGIO;
	info.si_int=1;
	info.si_code = SI_QUEUE;	

	printk("<1>IRQ received: %d\n", irq);
	printk("<1>searching for task id: %d\n", signal_pid);

	t= pid_task(find_vpid(signal_pid),PIDTYPE_PID);//user_pid has been fetched successfully

	if(t == NULL){
		printk("<1>no such pid, cannot send signal\n");
	} else {
		printk("<1>found the task, sending signal\n");
		send_sig_info(SIGIO, &info, t);
	}

	clearEventStatus(25);
	printk("<1>end IRQ handler\n");

	return IRQ_HANDLED;
}

void rgpio_enable_irq(int irq) {

	uint32_t *vbase_ptr;
	uint32_t reg2 = 0;
	uint32_t reg2_set = 0;

        if(irq < 32) {
		//(*reg1) = (uint32_t) (0x01 << irq);
	} else {
		printk("<1> about to allocate mem region");
		
		if( request_mem_region(INTERRUPT_BASE, INTERRUPT_MAPPED_SIZE, "rgpio") == NULL ) {
			printk("<1> could not reserve mem region");
		} else {
			printk("<1> reserved mem region");
		}
		
		vbase_ptr = (uint32_t *) ioremap(INTERRUPT_BASE, INTERRUPT_MAPPED_SIZE );
		reg2 = ioread32(vbase_ptr + INTERRUPT_REG_ENABLE2_OFFSET);	
		reg2_set = (0x01 << (irq - 32));
		printk("<1>about to set a value for reg2: 0x%08X", reg2_set);
		iowrite32(reg2_set, vbase_ptr + INTERRUPT_REG_ENABLE2_OFFSET);
		iounmap(vbase_ptr);
		release_mem_region(INTERRUPT_BASE, INTERRUPT_MAPPED_SIZE);
	}

	return;
}
